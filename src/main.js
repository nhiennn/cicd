import Vue from 'vue'
import App from './App.vue'

Vue.config.productionTip = false

import Highcharts from 'highcharts'
import HighchartsVue from 'highcharts-vue'
import loadHighchartsMore from 'highcharts/highcharts-more';

loadHighchartsMore(Highcharts)
Vue.use(HighchartsVue, {Highcharts})

new Vue({
  render: h => h(App),
}).$mount('#app')
